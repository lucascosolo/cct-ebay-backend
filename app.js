const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const { v4: uuidv4 } = require('uuid');

const app = express();
const PORT = process.env.PORT || 3000;
const DATABASE_FILE = '/var/lib/data/database.json';
const INITIAL_DATABASE_FILE = 'database.json';

const SETTINGS_FILE = '/var/lib/data/settings.json';
// Load settings
let settings;
try {
    settings = JSON.parse(fs.readFileSync(SETTINGS_FILE, 'utf8'));
} catch (error) {
    console.error('Failed to load settings:', error);
}

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json()); // for parsing application/json
app.use(express.static('public'));
app.set('view engine', 'ejs');

app.get('/', (req, res) => {
    const entries = load_data();
    res.render('index', { entries });
});

app.get('/manage/:password', (req, res) => {
    if (req.params.password === settings.password) {
        const entries = load_data();
        res.render('manage', { entries });
    } else {
        res.status(403).send('Forbidden');
    }
});

app.post('/add', (req, res) => {
    const entry = {
        id: uuidv4(),
        item_name: req.body.item_name,
        status: req.body.listed_price ? 'Listed' : 'Not Listed',
        timestamp: new Date().toLocaleString(),
        witness: req.body.witness,
        listed_price: req.body.listed_price || null,
        sold_price: null,
        checkout_date: req.body.checkout_date,
        listing_date: req.body.listing_date,
        return_date: req.body.return_date,
        sale_date: req.body.sale_date
    };
    update_data(entry);
    res.redirect('/manage/' + settings.password);
});

app.post('/update/:id', (req, res) => {
    const { id } = req.params;
    const { status, price } = req.body;
    updateStatus(id, status, price);
    res.sendStatus(200);
});

app.post('/delete/:id', (req, res) => {
    const { id } = req.params;
    deleteEntry(id);
    res.sendStatus(200);
});

function load_data() {
    try {
        // If the database file does not exist in the persistent disk, copy it from the working directory
        if (!fs.existsSync(DATABASE_FILE)) {
            fs.copyFileSync(INITIAL_DATABASE_FILE, DATABASE_FILE);
        }
        const data = fs.readFileSync(DATABASE_FILE, 'utf8');
        const entries = JSON.parse(data) || [];

        // Custom sorting function based on the desired order
        const order = ['Listed', 'Not Listed', 'Sold', 'Returned'];
        entries.sort((a, b) => {
            if (order.indexOf(a.status) === order.indexOf(b.status)) {
                return a.item_name.localeCompare(b.item_name);
            }
            return order.indexOf(a.status) - order.indexOf(b.status);
        });

        return entries;
    } catch (error) {
        return [];
    }
}

function update_data(entry) {
    const entries = load_data();
    entries.push(entry);
    fs.writeFileSync(DATABASE_FILE, JSON.stringify(entries, null, 2));
}

function updateStatus(entryId, newStatus, price) {
    const entries = load_data();
    const entryIndex = entries.findIndex(entry => entry.id === entryId);
    if (entryIndex !== -1) {
        entries[entryIndex].status = newStatus;
        entries[entryIndex].timestamp = new Date().toLocaleString();
        if (newStatus === 'Sold') {
            entries[entryIndex].sold_price = price;
        } else if (newStatus === 'Listed') {
            entries[entryIndex].listed_price = price;
        }
        fs.writeFileSync(DATABASE_FILE, JSON.stringify(entries, null, 2));
    }
}

function deleteEntry(entryId) {
    const entries = load_data();
    const updatedEntries = entries.filter(entry => entry.id !== entryId);
    fs.writeFileSync(DATABASE_FILE, JSON.stringify(updatedEntries, null, 2));
}

app.listen(PORT, () => {
    console.log(`Server is running on http://localhost:${PORT}`);
});
